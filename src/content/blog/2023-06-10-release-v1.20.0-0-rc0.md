---
title: Forgejo v1.20 release candidates
publishDate: 2023-06-10
tags: ['releases', 'rc']
release: 1.20.0-0-rc0
excerpt: The first Forgejo v1.20 release candidate is ready for testing. Discover a more stable and secure internal CI/CD with its documentation, new API features, numerous UI/UX changes and user blocking for self-moderation.
---

Today the first release candidate for the upcoming Forgejo v1.20 release [was published](https://codeberg.org/forgejo-experimental/forgejo/releases/tag/v1.20.0-0-rc0) and the [documentation](/docs/v1.20) updated. It is meant for testing only: **do not upgrade a
production instance with it**.

The highlights are:

- The [internal CI/CD](/docs/v1.20/user/actions) known as `Forgejo Actions` is now used by Forgejo for [testing and releasing](https://code.forgejo.org/forgejo/runner/src/branch/main/.forgejo/workflows) the [Forgejo Runner](https://code.forgejo.org/forgejo/runner/). It is still in its infancy and disabled by default but it is stable and secure enough to be activated on [Forgejo's own instance](https://code.forgejo.org). The documentation was updated to explain how to [install](/docs/v1.20/admin/actions) and [use](/docs/v1.20/user/actions) it.
- The User Interface (UI) and User eXperience (UX) changed significantly and will require some adjustment from users and admins who created their own customized templates.
- New API features and endpoints were added (activity feeds, renaming users, uploading file, retrieving commits, etc.).
- Essential sub-systems were refactored (the queue system that handles background tasks such as checking pull requests, the logger used to display Forgejo's logs, ...). In theory these changes are transparent to the Forgejo user and admin but the risk of subtle regressions is real: do not hesitate to [reach out](https://codeberg.org/forgejo/forgejo/issues) if you suspect anything.

The [draft release notes](https://codeberg.org/forgejo/forgejo/src/branch/forgejo/RELEASE-NOTES.md#draft-1-20-0-0) have a categorized list of the most significant changes:

- `[A11Y]` accessibility of the web interface.
- `[API]` REST API.
- `[AUTH]` authentication.
- `[CI]` Forgejo's own Continuous Integration.
- `[MODERATION]` moderation tools.
- `[PACKAGES]` package registries.
- `[REFACTOR]` internal refactors.
- `[RSS]` display and creation of RSS feeds.
- `[TEMPLATES]` templating system used to create web pages, issues, mails, etc.
- `[TIME]` time localization and readability.
- `[UI / UX]` User Interface and User eXperience.
- `[WEBHOOK]` webhooks and notifications triggered when something happens in Forgejo.
- `[WIKI]` wiki features.

Make sure to [check the breaking
changes](https://codeberg.org/forgejo/forgejo/src/branch/forgejo/RELEASE-NOTES.md#draft-1-20-0-0)
(look for the string `BREAKING`) and get your production instance
ready for when the v1.20 release is available.

There was progress regarding federation with the integration of the
[F3 driver](https://codeberg.org/forgejo/forgejo/commits/branch/forgejo-f3)
in the codebase (an essential building block to synchronize forges
with each other) but nothing is ready for experimenting yet.

### Try it out

The release candidate is published in [the dedicated "experimental"
Forgejo organization](https://codeberg.org/forgejo-experimental) and
can be downloaded from:

- Containers at https://codeberg.org/forgejo-experimental/-/packages/container/forgejo/1.20
- Binaries at https://codeberg.org/forgejo-experimental/forgejo/releases/tag/v1.20.0-0-rc0

Checkout the v1.20 documentation section for detailed
[installation instructions](/docs/v1.20/admin/installation).

It will be updated based on your feedback until it becomes robust enough to be released.

### Help write good release notes

The best release notes are meant to articulate the needs and benefits
of new features and the actions recommended for breaking changes so
Forgejo admins quickly know if it is of interest to them.

The [current draft release
notes](https://codeberg.org/forgejo/forgejo/src/branch/forgejo/RELEASE-NOTES.md#draft-1-20-0-0)
are still incomplete. They will be finished by the time the release is published
and you can help make them better.

### Contribute to Forgejo

If you have any feedback or suggestions for Forgejo, we'd love to hear from you!
Open an issue on [the issue tracker](https://codeberg.org/forgejo/forgejo/issues)
for feature requests or bug reports. You can also find us [on the Fediverse](https://floss.social/@forgejo),
or drop by [the Matrix space](https://matrix.to/#/#forgejo:matrix.org)
([main chat room](https://matrix.to/#/#forgejo-chat:matrix.org)) to say hi!
