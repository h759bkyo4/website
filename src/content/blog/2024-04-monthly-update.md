---
title: Forgejo monthly update - April 2024
publishDate: 2024-04-30
tags: ['news', 'report']
excerpt: Contributors got together to celebrate the release of Forgejo v7.0 and Codeberg was upgraded the next day. A lot of effort went into the automation of the development process, for dependency management and releases, so that contributors can focus on what matters most. As Forgejo matures, more and more of the work is about day to day management of bug reports, localization, security, etc. All aspects that make Forgejo a product that can be relied on for the years ahead. 17 interviews were conducted to better understand how Forgejo is used and shape its roadmap in a user centered way.
---

The monthly report is meant to provide a good overview of what has changed in Forgejo in the past month. If you would like to help, please get in touch in [the chatroom](https://matrix.to/#/#forgejo-chat:matrix.org) or participate in the [ongoing discussions](https://codeberg.org/forgejo/discussions).

Contributors got together to celebrate the release of [Forgejo v7.0](https://forgejo.org/2024-04-release-v7-0/) and Codeberg was upgraded the next day. A lot of effort went into the automation of the development process, for dependency management and releases, so that contributors can focus on what matters most. As Forgejo matures, more and more of the work is about day to day management of bug reports, localization, security, etc. All aspects that make Forgejo a product that can be relied on for the years ahead. 17 interviews were conducted to better understand how Forgejo is used and shape its roadmap in a user centered way.

## Forgejo 7.0

[Forgejo v7.0 was published 23 April](https://forgejo.org/2024-04-release-v7-0/) with translations in Bulgarian, Esperanto, Filipino and Slovenian; SourceHut builds integration; support for the SHA-256 hash function in Git; source code search by default and more. It also is the first Long Term Support version and will receive updates until July 2025. The adoption of semantic versioning is the reason for the version bump from v1.21 to v7.0 and is compatible with existing tools.

### Regressions and 7.0.1

Previous Forgejo releases were synchronized with the Gitea release cycle and usually published after the first patch release, when the most disruptive bugs or regressions were discovered and fixed.[Forgejo became a hard fork](https://forgejo.org/2024-02-forking-forward/) two months ago and it was its first major release. Two significant regressions were discovered less than 48h after the release. Their impact and workarounds were documented [by amending the 7.0.0 release notes](https://codeberg.org/forgejo/forgejo/src/branch/forgejo/RELEASE-NOTES.md#7-0-0) while the [7.0.1 patch release](https://codeberg.org/forgejo/forgejo/src/branch/forgejo/RELEASE-NOTES.md#7-0-1) was prepared. It was released four days later.

### Celebration and motivations

On 27 April Forgejo contributors got together in a videoconference to celebrate the publication of the 7.0 release and remember the evolution of the project since its inception in 2022. They also [shared their motivations and goals for 2024](https://codeberg.org/forgejo/discussions/issues/158) in a discussion for everyone to know what they find important, but also what motivates them to contribute to a given topic.

### Codeberg upgrade

The Codeberg staging instance was used with Forgejo 7.0 release candidates and a copy of the production data in an attempt to identify scaling issues and regressions.

The [Codeberg specific patches](https://codeberg.org/Codeberg-Infrastructure/forgejo/src/branch/codeberg-7) were ported (new templates and CSS [need to be adapted](https://forgejo.org/docs/latest/developer/customization/)) and Codeberg was migrated to 7.0 on 28 April.

### Release notes quality

In order to [improve the quality of the release notes](https://codeberg.org/forgejo/discussions/issues/155), a [new requirement](https://codeberg.org/forgejo/discussions/issues/159) to merging a pull request was discussed and led to [an agreement proposal](https://codeberg.org/forgejo/governance/issues/118). The author of a pull request would be required to provide a snippet if their change needs to show in the release notes.

## Backport automation

The [git-backporting](https://github.com/kiegroup/git-backporting/) action was [improved](https://github.com/kiegroup/git-backporting/compare/v4.6.0...v4.8.0) to support:

- multiple targets
- notifications on error
- merged and squash pull requests

As expected it is now used to [backport most pull requests](https://codeberg.org/forgejo/forgejo/pulls?poster=165271&state=closed) from the development branch to the `v7.0/forgejo` branch.

The release notes themselves that were previously only found in the development branch are now [also backported to the stable branch](https://codeberg.org/forgejo/forgejo/pulls/3489) where they belong.

## Code

https://codeberg.org/forgejo/forgejo

Notable improvements and bug fixes:

- [Add Option to hide Release Archive links](https://codeberg.org/forgejo/forgejo/pulls/3139)
- [Limit database max connections by default](https://codeberg.org/forgejo/forgejo/pulls/3383)
- [ldap synchronization can use a new field to make domain name configurable](https://codeberg.org/forgejo/forgejo/pulls/3414)

[Read more](https://codeberg.org/forgejo/forgejo/pulls?labels=209916) in the pull requests.

## Security

There is [no direct impact of the xz backdoor (CVE-2024-3094) on Forgejo](https://forgejo.org/2024-03-xz/). This CVE got a lot of attention and a blog post was published to explain why Forgejo is not affected.

The Forgejo v1.21.11-0 release [was published 18 April](https://forgejo.org/2024-04-release-v1-21-11-0/) and contains two security fixes: a privilege escalation that allows any registered user to change the visibility of any public repository; and a cross-site scripting (XSS) vulnerability that enabled attackers to run unsandboxed client-side scripts on pages served from the forge's domain.

## User Research

The work started early March on [Forgejo Usability](https://codeberg.org/forgejo/discussions/issues/130) and [Designing a modern contribution workflow](https://codeberg.org/forgejo/discussions/issues/131) led to 17 interviews [for which transcripts were archived](https://codeberg.org/forgejo/user-research/src/branch/main/interviews/2024-04). They provide material that can be re-used in various contexts, even for the benefit of forges other than Forgejo.

Some of the information already influenced decisions and implementation details in discussions, and [issues were created](https://codeberg.org/forgejo/forgejo/issues/?q=&type=all&state=open&labels=208298&milestone=0&assignee=0&poster=179) in response to the user interviews, for example to improve the new repo units button and feedback for features that are still in development, such as [improving assignment to issues](https://codeberg.org/forgejo/forgejo/issues/3502).

A discussion on how to [move on with user research](https://codeberg.org/forgejo/discussions/issues/156) was started.

A [survey started](https://codeberg.org/forgejo/discussions/issues/157) on **Exploring the Contributor Experience** where Forgejo contributors are asked to answer a series of questions.

## Dependency Management

The [dependency update dashboard](https://codeberg.org/forgejo/forgejo/issues/2779) is used daily to observe and update Forgejo dependencies. When a new release is available, [a daily scheduled workflow](https://codeberg.org/forgejo/forgejo/src/branch/forgejo/.forgejo/workflows/renovate.yml) will create a [pull request](https://codeberg.org/forgejo/forgejo/pulls?poster=165503) which:

- will be merged automatically, for instance if it is a patch upgrade from a dependency known to have a trusted release process
- be reviewed by a Forgejo contributor to decided if it worth an upgrade

With hundreds of dependencies, there is a significant backlog to absorb and it is done incrementally by improving the [configuration file](https://codeberg.org/forgejo/forgejo/src/branch/forgejo/renovate.json) with:

- groups of dependencies, for instance [postcss](https://codeberg.org/forgejo/forgejo/src/commit/27fa12427ce86cd81dbecd184b13a3dd3d7061d9/renovate.json#L7) where multiple dependencies are treated as one
- automerging [patch releases](https://codeberg.org/forgejo/forgejo/src/commit/27fa12427ce86cd81dbecd184b13a3dd3d7061d9/renovate.json#L80)
- adding [custom dependency detection](https://codeberg.org/forgejo/forgejo/src/commit/27fa12427ce86cd81dbecd184b13a3dd3d7061d9/renovate.json#L134)

When Forgejo became a hard fork two months ago, maintainers switched from rebasing weekly on top of Gitea to cherry picking commits instead. A [tool](https://codeberg.org/forgejo/tools) has been developed to make this process easier, and automate as much of it as possible. As the tool keeps evolving, the weekly cherry pick pull request become [easier to create](https://codeberg.org/forgejo/tools/issues/31#issuecomment-1792960), review, and even includes [interesting statistics](https://codeberg.org/forgejo/forgejo/pulls/3513) at the bottom of the summary.

## Forgejo v1.20 end of life

With the release of Forgejo 7.0, the Forgejo v1.20 release is EOL (End Of Life) and will no longer receive security patches. The last of them was [backported](https://codeberg.org/forgejo/forgejo/pulls/3319) and made available in the [v1.20/forgejo](https://codeberg.org/forgejo/forgejo/commits/branch/v1.20/forgejo) branch for the benefit of source builds in April.

The [end-to-end tests](https://code.forgejo.org/forgejo/end-to-end) were also [updated](https://code.forgejo.org/forgejo/end-to-end/pulls/167) to remove v1.20 support and [the policy to managed](https://code.forgejo.org/forgejo/end-to-end/src/branch/main/README.md#removing-legacy-tests) legacy tests was documented.

## Debian packages

The [Debian package](https://codeberg.org/forgejo-contrib/forgejo-deb) has undergone extensive changes. In addition to gaining support for Forgejo v7.0 LTS, its CI was [migrated to Forgejo Actions](https://codeberg.org/forgejo-contrib/forgejo-deb/src/branch/main/.forgejo/workflows/forgejo-deb.yml) and includes a pull request test suite. The repository now has an LTS release channel. Enhancements are in the works to remove Forgejo's common data from the compiled binaries and instead storing it in a forgejo-common package. This lays the groundwork for things like multi-architecture builds.

A [call for maintainers](https://codeberg.org/forgejo-contrib/forgejo-deb/issues/34) was posted so that there are at least three maintainers to ensure:

- Debian packages are available within 12 hours of Forgejo releases
- Pull requests can be reviewed by at least one maintainer

References:

- https://codeberg.org/forgejo-contrib/forgejo-deb

## Helm chart

The Forgejo helm chart had [one major updates](https://codeberg.org/forgejo-contrib/forgejo-helm/releases).

References:

- https://codeberg.org/forgejo-contrib/forgejo-helm/releases

## Localization

The localization keeps going forward. 1 member [was onboarded](https://codeberg.org/forgejo/governance/pulls/112) and 2 more applications were created. Some optimizations to the merge process were made to make it go faster. 6 batches of updates [were merged](https://codeberg.org/forgejo/forgejo/pulls?state=closed&poster=67160), containing total of 2273 new translations and 1913 improvements. The translation effort had finally seen the light with the Forgejo 7.0.0 release, which is the first release containing major Forgejo localization improvements.
There are still countless improvements to be made for many languages and you can help to improve the localization too. [Learn how](https://forgejo.org/docs/latest/developer/localization).

## Federation

The pull request to implement [federated stars](https://codeberg.org/forgejo/forgejo/pulls/1680) passes tests and is ready for merging. It is a very large pull request and it was requested by reviewers to split it into smaller, more manageable pull requests. In the same way the large webhook refactor was done. The first pull request [was open and is its final stage](https://codeberg.org/forgejo/forgejo/pulls/3494) to validate ActivityPub messages.

A new pull request to [implement federated search](https://codeberg.org/forgejo/forgejo/pulls/3128) was proposed with a demonstration searching for an actor on https://next.forgejo.org from a locally running Forgejo instance.

## Governance

### Sustainability

The [discussion started](https://codeberg.org/forgejo/discussions/issues/144) on Forgejo durability in the next 10 years led to drafting a grant proposal for the new [Free and Open Source Software Sustainability Fund](https://www.opentech.fund/funds/free-and-open-source-software-sustainability-fund/). It proposes the creation of a non-profit organization managed transparently so that it can be audited at any time. Its purpose would be to define a collective roadmap and use the grant to fund the work. To preserve the Forgejo dynamic that is key to its momentum, it will be setup to ensure volunteers keep being its driving force.

## We Forge

Forgejo is a **community of people** who contribute in an inclusive environment. We forge on an equal footing, by reporting a bug, voicing an idea in the chatroom or implementing a new feature. The following list of contributors is meant to reflect this diversity and acknowledge all contributions since the last monthly report was published. If you are missing, please [ask for an update](https://codeberg.org/forgejo/website/issues/new).

- https://codeberg.org/0ko
- https://codeberg.org/adaaa
- https://codeberg.org/algernon
- https://codeberg.org/Andre601
- https://codeberg.org/anri
- https://codeberg.org/AverageHelper
- https://codeberg.org/axd99
- https://codeberg.org/banaanihillo
- https://codeberg.org/bapt
- https://codeberg.org/BaumiCoder
- https://codeberg.org/bdr9
- https://codeberg.org/bdube
- https://codeberg.org/Beowulf
- https://codeberg.org/buhtz
- https://codeberg.org/ChrSt
- https://codeberg.org/clarfonthey
- https://codeberg.org/con-f-use
- https://codeberg.org/Crown0815
- https://codeberg.org/crystal
- https://codeberg.org/Cyborus
- https://codeberg.org/ddevault
- https://codeberg.org/deblan
- https://codeberg.org/Dirk
- https://codeberg.org/Drakon
- https://codeberg.org/earl-warren
- https://codeberg.org/el0n
- https://codeberg.org/eo
- https://codeberg.org/flipreverse
- https://codeberg.org/fnetX
- https://codeberg.org/foxy
- https://codeberg.org/Frankkkkk
- https://codeberg.org/gmask
- https://codeberg.org/grosmanal
- https://codeberg.org/Gusted
- https://codeberg.org/h759bkyo4
- https://codeberg.org/hazy
- https://codeberg.org/ikidd
- https://codeberg.org/iminfinity
- https://codeberg.org/intelfx
- https://codeberg.org/itsdrike
- https://codeberg.org/JakobDev
- https://codeberg.org/jean-daricade
- https://codeberg.org/JeremyStarTM
- https://codeberg.org/jerger
- https://codeberg.org/jfinkhaeuser
- https://codeberg.org/Justman10000
- https://codeberg.org/jwells
- https://codeberg.org/jwildeboer
- https://codeberg.org/KaKi87
- https://codeberg.org/kB01
- https://codeberg.org/kita
- https://codeberg.org/KlavsKlavsen
- https://codeberg.org/KOLANICH
- https://codeberg.org/lampajr
- https://codeberg.org/liberodark
- https://codeberg.org/Link1J
- https://codeberg.org/Mai-Lapyst
- https://codeberg.org/mainboarder
- https://codeberg.org/maltfield
- https://codeberg.org/markuzcha
- https://codeberg.org/matheusmoreira
- https://codeberg.org/maya
- https://codeberg.org/MichaelTen
- https://codeberg.org/mlncn
- https://codeberg.org/mnq
- https://codeberg.org/moonglum
- https://codeberg.org/n0toose
- https://codeberg.org/natct
- https://codeberg.org/nercon
- https://codeberg.org/nezbednik
- https://codeberg.org/NicolasCARPi
- https://codeberg.org/oliverpool
- https://codeberg.org/payas
- https://codeberg.org/Pi-Cla
- https://codeberg.org/PixelHamster
- https://codeberg.org/popey
- https://codeberg.org/programmerjake
- https://codeberg.org/rafadc
- https://codeberg.org/realaravinth
- https://codeberg.org/ReptoxX
- https://codeberg.org/saltstack-admin
- https://codeberg.org/sbatial
- https://codeberg.org/sergeyk
- https://codeberg.org/shanzez
- https://codeberg.org/silverwind
- https://codeberg.org/SinTan1729
- https://codeberg.org/snematoda
- https://codeberg.org/SnowCode
- https://codeberg.org/sosasees
- https://codeberg.org/SR-G
- https://codeberg.org/stsp
- https://codeberg.org/Sunner
- https://codeberg.org/tampler
- https://codeberg.org/thefox
- https://codeberg.org/theoryshaw
- https://codeberg.org/thepaperpilot
- https://codeberg.org/Thesola10
- https://codeberg.org/thomas-maurice
- https://codeberg.org/tmb
- https://codeberg.org/VehementHam
- https://codeberg.org/viceice
- https://codeberg.org/VicinityNeurosis
- https://codeberg.org/vsz
- https://codeberg.org/wangito33
- https://codeberg.org/wetneb
- https://codeberg.org/Wuzzy
- https://codeberg.org/Xinayder
- https://codeberg.org/yarikoptic
- https://codeberg.org/yumisea
- https://codeberg.org/zareck
- https://codeberg.org/zontreck
- https://codeberg.org/zotan
- https://codeberg.org/Zottelchen
- https://codeberg.org/zwanto
- https://translate.codeberg.org/user/747
- https://translate.codeberg.org/user/emansije
- https://translate.codeberg.org/user/Eriwi
- https://translate.codeberg.org/user/EssGeeEich
- https://translate.codeberg.org/user/FedericoSchonborn
- https://translate.codeberg.org/user/Fjuro
- https://translate.codeberg.org/user/FunctionalHacker
- https://translate.codeberg.org/user/furry
- https://translate.codeberg.org/user/hankskyjames777
- https://translate.codeberg.org/user/kdh8219
- https://translate.codeberg.org/user/kecrily
- https://translate.codeberg.org/user/leana8959
- https://translate.codeberg.org/user/lucasmz
- https://translate.codeberg.org/user/m0s
- https://translate.codeberg.org/user/Mormegil
- https://translate.codeberg.org/user/Mylloon
- https://translate.codeberg.org/user/Quitaxd
- https://translate.codeberg.org/user/rguards
- https://translate.codeberg.org/user/salif
- https://translate.codeberg.org/user/sinsky
- https://translate.codeberg.org/user/SteffoSpieler
- https://translate.codeberg.org/user/toasterbirb
- https://translate.codeberg.org/user/WithLithum
- https://translate.codeberg.org/user/yeziruo
- https://translate.codeberg.org/user/ZilloweZ
- https://translate.codeberg.org/user/Zughy

A **minority of Forgejo contributors earn a living** by implementing the roadmap co-created by the Forgejo community, see [the sustainability repository](https://codeberg.org/forgejo/sustainability) for the details.
